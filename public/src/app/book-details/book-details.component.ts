import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { BooksService } from '../books.service';
import { Book } from '../book';
@Component({
  selector: 'app-book-details',
  template: `
  <div class="details">
    <div><h3>Book Details</h3></div>
      <div class="details1">
        <div><h4><b>ISBN No : </b>&nbsp;&nbsp;&nbsp;{{book.isbn}}</h4></div>
        <div><h4><b>Title : </b>&nbsp;&nbsp;&nbsp;{{book.title}}</h4></div>
        <div><h4><b>Author : </b>&nbsp;&nbsp;&nbsp;{{book.author}}</h4></div>
        <div><h4><b>Description : </b>&nbsp;&nbsp;&nbsp;{{book.description}}</h4></div>
      </div>
      <div class="buttons">
        <div><button (click)="updateBook()"><i>&#x270d;</i></button></div>
        <div><button (click)="deleteBook()"><i>&#x2718;</i></button></div>
        <div><button (click)="gotoBooks()">Back</button></div>
    </div>
  </div>
  `,
  styles: [`
  div.details {
    color:rgb(236, 236, 220);
    box-sizing:border-box;
    border-radius: 20px;
    display: flex;
    flex-flow: column;
    margin: auto;
    margin-top: 3em;
    max-width: 600px;
    padding: 2em;
    background: rgba(0,0,0.5,0.5);
  }
  div b {
    color: yellow;
  }
  div.details1 {
    display: flex;
    flex-flow: column;
    justify-content: space-evenly;
  }
  div h3 {
    text-decoration: underline;
    color: Yellow;
    margin: auto;
    margin-top: 2em;
    display: flex;
    justify-content: space-evenly;
  }
  div.buttons{
    padding: 1em;
    justify-content: space-evenly;
    display: flex;
  }
  `]
})
export class BookDetailsComponent implements OnInit {

  // public books: Book[];
  public index;
  public book;

  constructor(private aroute: ActivatedRoute,
    private router: Router,
    private booksService: BooksService) { }

  ngOnInit() {
    this.aroute.paramMap.subscribe((params: ParamMap) => {
      this.index = parseInt(params.get('i'), 10);
      this.book = this.booksService.getBook(this.index);
    });
  }

  updateBook() {
    this.router.navigate(['/bookUpdate', {'index': this.index}]);
  }

  gotoBooks() {
    this.router.navigate(['/books']);
  }

  deleteBook() {
    this.booksService.deleteBook(this.index);
    this.router.navigate(['/books']);
  }

}
