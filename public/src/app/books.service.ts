import { Injectable } from '@angular/core';
import { Book } from './book';

@Injectable({
  providedIn: 'root'
})

export class BooksService {

  constructor() { }
  public books = [
    {isbn: '101', title: 'Nakuthanthi', author: 'D.R Bendre', description: 'About Life'}
];

  getBooks() {
    return this.books;
  }

  getBook(index: number): Book | undefined {
    if (index < this.books.length) {
      return this.books[index];
    }
  }

  addBook(book) {
    this.books.push(book);
  }

  deleteBook(index) {
    this.books.splice(index, 1);
  }

}
