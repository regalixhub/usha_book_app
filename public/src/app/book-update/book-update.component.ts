import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BooksService } from '../books.service';
import { Book } from '../book';

@Component({
  selector: 'app-book-update',
  template: `<div class="edit">
  <label>ISBN No:</label><input [(ngModel)]="book.isbn" type="text"><br><br>
  <label>Title:</label><input [(ngModel)]="book.title" type="text"><br><br>
  <label>Author:</label><input [(ngModel)]="book.author" type="text"><br><br>
  <label>Desc:</label><input [(ngModel)]="book.description" type="text"><br><br>
  <div class="buttons"><button (click)="updateBook()">Save</button>
  <button (click)="cancel()" >Cancel</button></div>
  </div>
  `,
  styles: [`
  div.edit {
    color:rgb(236, 236, 220);
    box-sizing:border-box;
    border-radius: 20px;
    display: flex;
    flex-flow: column;
    margin: auto;
    margin-top: 3em;
    max-width: 600px;
    padding: 2em;
    background: rgba(0,0,0.5,0.5);
  }

  div.buttons{
    padding: 1em;
    justify-content: space-evenly;
    display: flex;
  }

  `]
})
export class BookUpdateComponent implements OnInit {

  public book: Book;
  public newBook: Book;
  public index: number;

  constructor(private router: Router,
    private aroute: ActivatedRoute,
    private booksService: BooksService) { }

  ngOnInit() {
    this.aroute.paramMap.subscribe((params: ParamMap) => {
      this.index = parseInt(params.get('index'), 10);
      this.book = this.booksService.getBook(this.index);
      this.newBook = new Book(this.book.isbn,
        this.book.title,
        this.book.author,
        this.book.description);
    });
  }

  updateBook() {
    this.router.navigate(['/bookUpdate']);
  }

  gotoBooks() {
    this.router.navigate(['/books']);
  }

  cancel() {
    const books = this.booksService.getBooks();
    books[this.index] = this.newBook;
    this.router.navigate(['/books']);
  }

}
