import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-book-add',
  template: `<div class="adds"><form #bookDetails="ngForm" (ngSubmit)="addBook(bookDetails.value)"><br>
    <div class="l1"><label> ISBN-No:</label><input type="text" name="isbn"  ngModel></div><br>
    <div class="l1"><label> Title:</label><input type="text" name="title" ngModel></div><br>
    <div class="l1"><label> Author:</label><input type="text" name="author" ngModel></div><br>
    <div class="l1"><label>Description:</label><input type="text" name="description" ngModel></div><br>
    <div class="buttons"><button type="submit">Add</button></div>
  </form></div>
  `,
  styles: [`
  div.adds {
    padding: 1em;
    color:rgb(236, 236, 220);
    background: rgba(0, 0, 0.5, 0.5);
    box-sizing:border-box;
    border-radius: 20px;
    display: flex;
    flex-flow: row;
    margin: auto;
    margin-top: 3em;
    justify-content: space-evenly;
    max-width: 600px;
  }
  div.l1{
    dispaly: flex;
    flex-flow: row;
    justify-content: space-between;
  }
  div.buttons{
    padding: 1em;
    justify-content: space-evenly;
    display: flex;
  }
  `]
})
export class BookAddComponent {

  constructor(private router: Router,
    private aroute: ActivatedRoute,
    private booksServices: BooksService) { }

  addBook(book) {
    if (book.isbn === '' || book.title === ''
    || book.author === '' || book.description === '') {
      alert('Please fill all Fields');
      return false;
    }

    this.booksServices.addBook(book);
    this.router.navigate(['/books']);
  }

  gotoBooks() {
    this.router.navigate(['/books']);
  }

}
