import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-book-list',
  template: `
  <div *ngIf="books.length==0; then message; else table"></div>
  <div>
    <form>
    <div class="heading"><h3>Books</h3></div>
    <div class="lists" *ngFor="let book of books; index as i">
        <div><h4 (click)="bookDetail(i)">{{book.isbn}}</h4></div>
        <div><h4 (click)="bookDetail(i)">{{book.title}}</h4></div>
        <div><button class="buttons" (click)="updateBook(i)"><i>&#x270d;</i></button></div>
        <div><button class="buttons" (click)="deleteBook(i)"><i>&#x2718;</i></button></div>
    </div>
    </form>
  </div>
  <div class="buttons"><button class="add" (click)="addNewBook()">Add Books</button></div>
  `,
  styles: [`
  div.heading {
    color: white;
    background: rgba(0, 0, 0.5, 0.5);
    box-sizing:border-box;
    border-radius: 20px;
    display: flex;
    flex-flow: row;
    margin: auto;
    margin-top: 2em;
    justify-content: space-evenly;
    max-width: 200px;
  }
  div.lists {
    padding: 1em;
    color: Yellow;
    text-decoration: none;
    box-sizing:border-box;
    border-radius: 20px;
    display: flex;
    flex-flow: row;
    margin: auto;
    margin-top: 2em;
    justify-content: space-evenly;
    max-width: auto;
    text-align: center;
  }
  div.buttons{
    padding: 1em;
    box-sizing:border-box;
    border-radius: 20px;
    display: flex;
    flex-flow: row;
    margin: auto;
    margin-top: 2em;
    justify-content: space-evenly;
    max-width: 200px;
  }
  `]
})
export class BookListComponent implements OnInit {

  public books = [];

  constructor(private router: Router,
    private aroute: ActivatedRoute,
    private booksService: BooksService) { }

  ngOnInit() {
    this.books = this.booksService.getBooks();
  }

  addNewBook() {
    this.router.navigate(['/addBooks']);
  }

  updateBook(i) {
    this.router.navigate(['/bookUpdate', {'index': i}]);
  }

  deleteBook(i) {
    this.booksService.deleteBook(i);
  }

  bookDetail(i) {
    this.router.navigate(['/bookDetails', i]);
  }

  gotoBooks() {
    this.router.navigate(['/books']);
  }

}
